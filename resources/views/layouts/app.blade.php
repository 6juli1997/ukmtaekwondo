<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="html 5 template">
    <meta name="author" content="">
    <title>Taekwondo UNY</title>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="stylesheet" href="{{asset('findhouses/css/jquery-ui.css')}}">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i%7CMontserrat:600,800" rel="stylesheet">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="{{asset('findhouses/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/fontawesome-5-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/font-awesome.min.css')}}">
    <!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" href="{{asset('findhouses/revolution/css/settings.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/revolution/css/layers.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/revolution/css/navigation.css')}}">
    <!-- ARCHIVES CSS -->
    <link rel="stylesheet" href="{{asset('findhouses/css/slider-home18.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/search.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/lightcase.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/owl-carousel.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/menu.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('findhouses/css/styles.css')}}">
    <link rel="stylesheet" id="color" href="{{asset('findhouses/css/colors/light-black.css')}}">
</head>

<body class="int_white_bg h19 homepage-5">
    <!-- Wrapper -->
    <div id="wrapper" class="int_main_wraapper">
        @include('layouts.navbar')

        @yield('content')

        @include('layouts.footer')

        <!--register form -->
        <div class="login-and-register-form modal">
            <div class="main-overlay"></div>
            <div class="main-register-holder">
                <div class="main-register fl-wrap">
                    <div class="close-reg"><i class="fa fa-times"></i></div>
                    <h3>Welcome to <span>Find<strong>Houses</strong></span></h3>
                    <div class="soc-log fl-wrap">
                        <p>Login</p>
                        <a href="#" class="facebook-log"><i class="fa fa-facebook-official"></i>Log in with Facebook</a>
                        <a href="#" class="twitter-log"><i class="fa fa-twitter"></i> Log in with Twitter</a>
                    </div>
                    <div class="log-separator fl-wrap"><span>Or</span></div>
                    <div id="tabs-container">
                        <ul class="tabs-menu">
                            <li class="current"><a href="#tab-1">Login</a></li>
                            <li><a href="#tab-2">Register</a></li>
                        </ul>
                        <div class="tab">
                            <div id="tab-1" class="tab-contents">
                                <div class="custom-form">
                                    <form method="post" name="registerform">
                                        <label>Username or Email Address * </label>
                                        <input name="email" type="text" onClick="this.select()" value="">
                                        <label>Password * </label>
                                        <input name="password" type="password" onClick="this.select()" value="">
                                        <button type="submit" class="log-submit-btn"><span>Log In</span></button>
                                        <div class="clearfix"></div>
                                        <div class="filter-tags">
                                            <input id="check-a" type="checkbox" name="check">
                                            <label for="check-a">Remember me</label>
                                        </div>
                                    </form>
                                    <div class="lost_password">
                                        <a href="#">Lost Your Password?</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab">
                                <div id="tab-2" class="tab-contents">
                                    <div class="custom-form">
                                        <form method="post" name="registerform" class="main-register-form" id="main-register-form2">
                                            <label>First Name * </label>
                                            <input name="name" type="text" onClick="this.select()" value="">
                                            <label>Second Name *</label>
                                            <input name="name2" type="text" onClick="this.select()" value="">
                                            <label>Email Address *</label>
                                            <input name="email" type="text" onClick="this.select()" value="">
                                            <label>Password *</label>
                                            <input name="password" type="password" onClick="this.select()" value="">
                                            <button type="submit" class="log-submit-btn"><span>Register</span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--register form end -->

        <!-- START PRELOADER -->
        <div id="preloader">
            <div id="status">
                <div class="status-mes"></div>
            </div>
        </div>
        <!-- END PRELOADER -->

        <!-- ARCHIVES JS -->
        <script src="{{asset('findhouses/js/jquery-3.5.1.min.js')}}"></script>
        <script src="{{asset('findhouses/js/jquery-ui.js')}}"></script>
        <script src="{{asset('findhouses/js/tether.min.js')}}"></script>
        <script src="{{asset('findhouses/js/moment.js')}}"></script>
        <script src="{{asset('findhouses/js/transition.min.js')}}"></script>
        <script src="{{asset('findhouses/js/transition.min.js')}}"></script>
        <script src="{{asset('findhouses/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('findhouses/js/mmenu.min.js')}}"></script>
        <script src="{{asset('findhouses/js/mmenu.js')}}"></script>
        <script src="{{asset('findhouses/js/swiper.min.js')}}"></script>
        <script src="{{asset('findhouses/js/swiper.js')}}"></script>
        <script src="{{asset('findhouses/js/slick.min.js')}}"></script>
        <script src="{{asset('findhouses/js/slick.js')}}"></script>
        <script src="{{asset('findhouses/js/fitvids.js')}}"></script>
        <script src="{{asset('findhouses/js/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('findhouses/js/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('findhouses/js/imagesloaded.pkgd.min.js')}}"></script>
        <script src="{{asset('findhouses/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('findhouses/js/smooth-scroll.min.js')}}"></script>
        <script src="{{asset('findhouses/js/lightcase.js')}}"></script>
        <script src="{{asset('findhouses/js/search.js')}}"></script>
        <script src="{{asset('findhouses/js/owl.carousel.js')}}"></script>
        <script src="{{asset('findhouses/js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('findhouses/js/ajaxchimp.min.js')}}"></script>
        <script src="{{asset('findhouses/js/newsletter.js')}}"></script>
        <script src="{{asset('findhouses/js/jquery.form.js')}}"></script>
        <script src="{{asset('findhouses/js/jquery.validate.min.js')}}"></script>
        <script src="{{asset('findhouses/js/searched.js')}}"></script>
        <script src="{{asset('findhouses/js/forms-2.js')}}"></script>
        <script src="{{asset('findhouses/js/color-switcher.js')}}"></script>
        <script>
            $(window).on('scroll load', function() {
                $("#header.cloned #logo img").attr("src", $('#header #logo img').attr('data-sticky-logo'));
            });

        </script>

        <!-- Slider Revolution scripts -->
        <script src="{{asset('findhouses/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
        <script src="{{asset('findhouses/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

        <script>
            var tpj = jQuery;
            var revapi26;
            tpj(document).ready(function() {
                if (tpj("#rev_slider_26_1").revolution == undefined) {
                    revslider_showDoubleJqueryError("#rev_slider_26_1");
                } else {
                    revapi26 = tpj("#rev_slider_26_1").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "revolution/js/",
                        sliderLayout: "fullscreen",
                        dottedOverlay: "none",
                        delay: 9000,
                        navigation: {
                            keyboardNavigation: "off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "off",
                            touch: {
                                touchenabled: "on",
                                touchOnDesktop: "off",
                                swipe_threshold: 75,
                                swipe_min_touches: 50,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            },

                            arrows: {
                                style: "metis",
                                enable: true,
                                hide_onmobile: false,
                                hide_onleave: false,
                                tmp: '',
                                left: {
                                    h_align: "right",
                                    v_align: "bottom",
                                    h_offset: 80,
                                    v_offset: 10
                                },
                                right: {
                                    h_align: "right",
                                    v_align: "bottom",
                                    h_offset: 10,
                                    v_offset: 10
                                }
                            },
                            bullets: {
                                enable: false,
                                hide_onmobile: false,
                                style: "bullet-bar",
                                hide_onleave: false,
                                direction: "horizontal",
                                h_align: "right",
                                v_align: "bottom",
                                h_offset: 30,
                                v_offset: 30,
                                space: 5,
                                tmp: ''
                            }
                        },
                        responsiveLevels: [1240, 1024, 778, 480],
                        visibilityLevels: [1240, 1024, 778, 480],
                        gridwidth: [1270, 1024, 778, 480],
                        gridheight: [729, 600, 600, 480],
                        lazyType: "none",
                        parallax: {
                            type: "scroll",
                            origo: "slidercenter",
                            speed: 2000,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
                        },
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        fullScreenAutoWidth: "off",
                        fullScreenAlignForce: "off",
                        fullScreenOffsetContainer: ".site-header",
                        fullScreenOffset: "0px",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }
            }); /*ready*/

        </script>
        
        <script>
            $('.home5-right-slider').owlCarousel({
                loop: true,
                margin: 30,
                dots: false,
                nav: true,
                rtl: false,
                autoplayHoverPause: false,
                autoplay: false,
                singleItem: true,
                smartSpeed: 1200,
                navText: ["<i class='fas fa-long-arrow-alt-left'></i>", "<i class='fas fa-long-arrow-alt-right'></i>"],
                responsive: {
                    0: {
                        items: 1,
                        center: false
                    },
                    480: {
                        items: 1,
                        center: false
                    },
                    520: {
                        items: 2,
                        center: false
                    },
                    600: {
                        items: 2,
                        center: false
                    },
                    768: {
                        items: 2
                    },
                    992: {
                        items: 3
                    },
                    1200: {
                        items: 5
                    },
                    1366: {
                        items: 5
                    },
                    1400: {
                        items: 5
                    }
                }
            });

        </script>

        <!-- MAIN JS -->
        <script src="{{asset('findhouses/js/script.js')}}"></script>

    </div>
    <!-- Wrapper / End -->
</body>

</html>
