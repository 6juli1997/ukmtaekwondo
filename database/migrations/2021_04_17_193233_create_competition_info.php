<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('competition_id');
            $table->string('kelas', 50)->nullable();
            $table->string('peringkat', 50);
            $table->string('nama', 100);
            $table->text('photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_info');
    }
}
