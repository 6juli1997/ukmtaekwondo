<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama', 100);
            $table->string('nim', 50);
            $table->string('jurusan', 100);
            $table->string('contact_person', 50);
            $table->string('tempat_lahir', 100);
            $table->dateTime('tanggal_lahir');
            $table->text('alasan_mendaftar');
            $table->string('berat_badan', 3);
            $table->string('tinggi_badan', 4);
            $table->string('sabuk', 30);
            $table->text('photo')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
